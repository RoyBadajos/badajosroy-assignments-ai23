<?php
/**
 * Template part for displaying slider posts
 */
?>
          	<div class="item tranz">
                    
                <div class="entryhead">
                    
                <?php echo lookout_icon();?>
                
                	<div class="icon-rating tranz">
            
                    	<?php if (function_exists('wp_review_show_total')) wp_review_show_total(); ?>
                    
                    </div>
                              
					<?php if ( has_post_thumbnail()){ ?>

                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail('lookout_big',array('class' => 'tranz grayscale grayscale-fade'));  ?>
                        </a>
        
                    <?php } ?>
                
                </div><!-- end .entryhead -->
                
                
                
                <div class="flexinside">
                
                	<?php lookout_meta();lookout_meta_counter();?> 
                
                    <h2><a class="link link--forsure" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2> 
                    
                     <?php lookout_meta_author(); ?>
        
        		</div>
        
            </div>