<?php
/**
 * Template part for displaying homepage posts
 */
?>
          	<div <?php post_class('item grid-item tranz'); ?>>
  				
                <?php lookout_meta_cat();?>
     
                <div class="entryhead">
                    
                   	<?php echo lookout_icon();?>
                
                	<div class="icon-rating tranz">
            
                    	<?php if (function_exists('wp_review_show_total')) wp_review_show_total(); ?>
                    
                    </div>
    
					<?php if ( has_post_thumbnail()){ ?>
                    
                        <div class="imgwrap">
							
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail('lookout_small',array('class' => 'tranz standard grayscale grayscale-fade'));  ?>
                            </a>
                        
                        </div>
    
                    <?php } else { } ?> 
                
                </div><!-- end .entryhead -->
    
            	<div class="item_inn tranz ghost p-border">
                
                	<?php lookout_meta_date();?>
        
                    <h2 class="posttitle"><a class="link link--forsure" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    
					<?php the_excerpt(); ?>
                    
                    <?php lookout_meta_more(); ?>
                
                </div><!-- end .item_inn -->
        
            </div>