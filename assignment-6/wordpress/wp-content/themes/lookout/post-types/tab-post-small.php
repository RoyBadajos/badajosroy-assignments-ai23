<?php
/**
 * Template part for displaying posts in featered widget
 */
?>
<div class="tab-post p-border ghost">

	<?php if ( has_post_thumbnail()) : ?>
    
        <div class="imgwrap">
        
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" >
            
              <?php the_post_thumbnail( 'lookout_tabs',array('class' => "grayscale grayscale-fade")); ?>
              
            </a>
        
        </div>
         
    <?php endif; ?>
        
    <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
    
	<?php lookout_meta_date();  ?>

</div>