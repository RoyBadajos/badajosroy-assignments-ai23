<?php
/**
 * The template for displaying archive pages
 */

get_header(); ?>
    
<div class="container builder woocommerce">

<div id="core" class="blog_builder">

	<div id="content" class="eightcol first">
    
            <?php if (is_category()) { ?>
    			<h2 class="archiv ghost"><span class="maintitle"><?php single_cat_title(); ?></span>
    			<span class="subtitle"><?php echo esc_html( strip_tags(category_description() ) ); ?> </span></h2>     
        
            <?php } elseif (is_day()) { ?>
            
    			<h2 class="archiv ghost"><span class="maintitle"><?php the_time( get_option( 'date_format' ) ); ?></span>
    			<span class="subtitle"><?php esc_html_e('Archive','lookout');?></span></h2>  

            <?php } elseif (is_month()) { ?>
            
    			<h2 class="archiv ghost"><span class="maintitle"><?php the_time( 'F, Y' ); ?></span>
    			<span class="subtitle"><?php esc_html_e('Archive','lookout');?></span></h2>  

            <?php } elseif (is_year()) { ?>
            
    			<h2 class="archiv ghost"><span class="maintitle"><?php the_time( 'Y' ); ?></span>
    			<span class="subtitle"><?php esc_html_e('Archive','lookout');?></span></h2>  

            <?php } elseif (is_author()) { ?>
            
    			<h2 class="archiv ghost"><span class="maintitle"><?php  $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); echo esc_html( $curauth->nickname );?></span>
                <span class="subtitle"><?php esc_html_e( 'Author','lookout' ); ?></span></h2>  
                <div class="authorpage ghost">
                    <?php echo esc_html( $curauth->user_description );  ?>
                    
              </div>
                
            <?php } elseif (is_tag()) { ?>
            
    			<h2 class="archiv ghost"><span class="maintitle"><?php echo single_tag_title( '', true); ?></span>
    			<span class="subtitle"><?php esc_html_e('Tag Archive','lookout');?></span></h2>  
            
            <?php } ?>
    
    
    	<?php if (have_posts()) : ?>

          <div class="blogger grid imgsmall">
                                        
                    <?php while (have_posts()) : the_post(); 
					
						get_template_part('/post-types/home-small');
                    
					endwhile; ?><!-- end post -->
                    
           	</div><!-- end latest posts section-->
            
            <div class="clearfix"></div>

					<div class="pagination"><?php the_posts_pagination(); ?></div>

					<?php else : ?>
			

                            <div class="errorentry entry">
                
                                <h1 class="post entry-title" itemprop="headline"><?php esc_html_e('Nothing found here','lookout');?></h1>
                            
                                <h4><?php esc_html_e('Perhaps You will find something interesting from these lists...','lookout');?></h4>
                            
                                <div class="hrline"></div>
                            
                                <?php get_template_part('inc/404-content');?>
                            
                            </div>
                        
                        
                        </div><!-- end latest posts section-->
                        
                        
					<?php endif; ?>
    
    </div><!-- end #content -->
    
    
    <?php get_sidebar(); ?>
    
	<div class="clearfix"></div>
    
</div><!-- end #core -->

<div class="clearfix"></div>

<?php get_footer(); ?>