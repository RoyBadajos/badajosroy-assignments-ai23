<?php
/**
 * The template for displaying content of the single post
 */
?>

<div <?php post_class('item normal tranz ghost p-border'); ?>>

    <h1 class="entry-title" itemprop="headline"><span itemprop="name"><?php the_title(); ?></span></h1>
    
    <?php if ( has_post_thumbnail()) : ?>

    <div class="entryhead" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
        
        <a href="<?php the_permalink(); ?>">
        
        	<?php the_post_thumbnail('lookout_slider',array('class' => 'standard grayscale grayscale-fade'));  ?>
            
        </a>
    
    </div><!-- end .entryhead -->
    
    <?php endif; ?>
 
              
    <div class="clearfix"></div>
    
    <div class="item_inn tranz p-border">
    
        <div class="meta-single p-border">
  				
			<?php lookout_meta_cat();?>
            
            <?php lookout_meta_author(); lookout_meta_date();?>
            
        </div>
        
        <div class="clearfix"></div>
                             
        <div class="entry" itemprop="text">
              
            <?php 
            
                the_content(); 
				
                echo '<div class="post-pagination">';
                wp_link_pages( array( 'before' => '<div class="page-link">', 'after' => '</div>',
				'link_before' => '<span>', 'link_after' => '</span>', ) );
				wp_link_pages(array(
					'before' => '<p>',
					'after' => '</p>',
					'next_or_number' => 'next_and_number', # activate parameter overloading
					'nextpagelink' => esc_html__('Next','lookout'),
					'previouspagelink' => esc_html__('Previous','lookout'),
					'pagelink' => '%',
					'echo' => 1 )
				);
				echo '</div>';
            
            ?>
            
            <div class="clearfix"></div>
            
        </div><!-- end .entry -->
        
            <?php 
				
                get_template_part('/single-info');
                
                comments_template(); 
                
            ?>
        
	</div><!-- end .item_inn -->
      
</div>