<?php
/**
 * Template part for displaying social icon section
 */
?>
<ul class="social-menu">

<?php if(get_theme_mod('lookout_facebook')) { ?>
<li class="sprite-facebook">
	<a title="<?php esc_html_e('Facebook','lookout');?>" href="<?php echo esc_url(get_theme_mod('lookout_facebook'));?>"><i class="fa fa-facebook-official"></i><span><?php esc_html_e('Facebook','lookout');?></span></a>
</li><?php } ?>

<?php if (get_theme_mod('lookout_twitter') )  { ?>
<li class="sprite-twitter">
	<a title="<?php esc_html_e('Twitter','lookout');?>" href="<?php echo esc_url(get_theme_mod('lookout_twitter'));?>"><i class="fa fa-twitter"></i><span><?php esc_html_e('Twitter','lookout');?></span></a>
</li><?php } ?>

<?php if (get_theme_mod('lookout_google') )  { ?>
<li class="sprite-google">
	<a title="<?php esc_html_e('Google+','lookout');?>" href="<?php echo esc_url(get_theme_mod('lookout_google'));?>"><i class="fa fa-google-plus"></i><span><?php esc_html_e('Google+','lookout');?></span></a>
</li><?php } ?>

<?php if (get_theme_mod('lookout_instagram') )  { ?>
<li class="sprite-instagram">
	<a title="<?php esc_html_e('Instagram','lookout');?>" href="<?php echo esc_url(get_theme_mod('lookout_instagram'));?>"><i class="fa fa-instagram"></i><span><?php esc_html_e('Instagram','lookout');?></span></a>
</li><?php } ?>

<?php if (get_theme_mod('lookout_pinterest') )  { ?>
<li class="sprite-pinterest">
	<a title="<?php esc_html_e('Pinterest','lookout');?>" href="<?php echo esc_url(get_theme_mod('lookout_pinterest'));?>"><i class="fa fa-pinterest-square"></i><span><?php esc_html_e('Pinterest','lookout');?></span></a>
</li><?php } ?>

<?php if (get_theme_mod('lookout_youtube') )  { ?>
<li class="sprite-youtube">
	<a title="<?php esc_html_e('You Tube','lookout');?>" href="<?php echo esc_url(get_theme_mod('lookout_youtube'));?>"><i class="fa fa-youtube-play"></i><span><?php esc_html_e('You Tube','lookout');?></span></a>
</li><?php } ?>

<?php if (get_theme_mod('lookout_linkedin') )  { ?>
<li class="sprite-linkedin">
	<a title="<?php esc_html_e('LinkedIn','lookout');?>" href="<?php echo esc_url(get_theme_mod('lookout_linkedin'));?>"><i class="fa fa-linkedin-square"></i><span><?php esc_html_e('LinkedIn','lookout');?></span></a>
</li><?php } ?>

<li class="searchicon"><a class="searchOpen" href="#" ><i class="fa fa-search"></i></a></li>

</ul>