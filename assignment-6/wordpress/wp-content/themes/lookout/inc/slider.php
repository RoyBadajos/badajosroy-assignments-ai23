<?php
/**
 * Template part for displaying main slider
 */
?>
<?php
    $lookout_catID = get_theme_mod('lookout_slider_category');
	
	if (isset($lookout_catID ) ? $lookout_catID : null) { ?>

      <div class="mainflex flexslider loading">
      
      <div class="loading-inn"><i class="fa fa-circle-o-notch fa-spin"></i></div>
  
          <?php 
          $my_query = new WP_Query('showposts=4&cat='. $lookout_catID .'');	 
          if ($my_query->have_posts()) :
          
          wp_enqueue_script('jquery-flexslider', get_template_directory_uri() .'/js/jquery.flexslider.js',array( 'jquery' ),'', true);		
          wp_enqueue_script('tmnf-flexslider.start.main', get_template_directory_uri() .'/js/jquery.flexslider.start.main.js',array( 'jquery' ),'', true);
          ?>
          
                <ul class="slides">
                <?php while ($my_query->have_posts()) : $my_query->the_post();$do_not_duplicate = $post->ID; ?>	
                
                    <li>
                        <?php get_template_part('/post-types/slider-post' ); ?>        
                    </li>       
            
                <?php  endwhile; wp_reset_postdata(); ?>
                </ul>
                
                        
            <?php endif; ?>
        
                <ul class="slide-nav">
                    
                    <?php while ($my_query->have_posts()) : $my_query->the_post();$do_not_duplicate = $post->ID; ?>	
                            
                         <li>
                            
                            <a class="islink" href="#" title="<?php the_title(); ?>">
                            
                                <span class="title"><?php the_title(); ?></span>
                                
                            </a>
                            
                         </li>
                    
                    <?php  endwhile; wp_reset_postdata(); ?>
                    
                </ul>    
  
      </div><!-- end .mainflex -->


	<?php } ?> 