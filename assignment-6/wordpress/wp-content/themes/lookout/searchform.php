<?php
/**
 * Template for displaying search forms 
 */
?>

<form class="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
<input type="text" name="s" class="s ghost rad p-border" size="30" value="<?php esc_attr_e('Search...','lookout'); ?>" onfocus="if (this.value = '') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php esc_attr_e('Search...','lookout'); ?>';}" />
<button class='searchSubmit ribbon' ><i class="fa fa-search"></i></button>
</form>