=== Lookout ===
Contributors: Dannci & wpmasters
Requires at least: WordPress 4.7
Tested up to: WordPress 4.9.1
Version: 1.0.7
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: two-columns, custom-background, custom-header, custom-menu, editor-style, grid-layout, blog, news, featured-images, flexible-header, custom-colors, full-width-template, sticky-post, threaded-comments, translation-ready, theme-options, one-column, left-sidebar, right-sidebar, three-columns


== Description ==
Lookout - a nice WordPress theme developed specifically for personal, streamlined blog websites. This theme is easy-to-use and brings fully responsive layout to your site. Lookout supports the awesome Theme Customizer for all theme settings and enables to create your own color scheme.  
For more information about Lookout please go to https://wpmasters.org/downloads/lookout-theme/.


== Installation ==
1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Lookout in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel and customize to taste.


== Copyright ==
Lookout WordPress Theme, Copyright 2017 Dannci
Lookout is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

Lookout bundles the following third-party resources:

jQuery FlexSlider , Copyright 2012 WooThemes
License: GNU General Public License v2.0
Source: https://github.com/woocommerce/FlexSlider

jQuery ScrollToFixed, Copyright 2011 Joseph Cava-Lynch
License: MIT
Source: https://github.com/bigspotteddog/ScrollToFixed

Font Awesome
    Font License
        Applies to all desktop and webfont files in the following directory: fonts/.
        License: SIL OFL 1.1
        URL: http://scripts.sil.org/OFL
    Code License
        Applies to styles/font-awesome.min.css
        License: MIT License
        URL: http://opensource.org/licenses/mit-license.html

Images in the screenshot:

    https://pixabay.com/en/fashion-fashionable-hairdo-man-1866574/
	License: https://creativecommons.org/publicdomain/zero/1.0/deed.en

    https://pixabay.com/en/antique-background-black-camera-2688767/
	License: https://creativecommons.org/publicdomain/zero/1.0/deed.en

    https://pixabay.com/en/man-portrait-black-and-white-1209947/
	License: https://creativecommons.org/publicdomain/zero/1.0/deed.en

    https://pixabay.com/en/photographer-tripod-camera-819365/
	License: https://creativecommons.org/publicdomain/zero/1.0/deed.en

    https://pixabay.com/en/burano-venice-italy-channel-water-2444224/
	License: https://creativecommons.org/publicdomain/zero/1.0/deed.en


== Changelog ==

= 1.0.7 =
* Released: January 26, 2018
- major changes to meet approval requirements

= 1.0.6 =
* Released: January 25, 2018
- README update

= 1.0.5 =
* Released: January 24, 2018
- changes to meet approval requirements

= 1.0.4 =
* Released: January 23, 2018
- major changes to meet approval requirements

= 1.0.3 =
* Released: January 23, 2018
- major changes to meet approval requirements

= 1.0.2 =
* Released: January 12, 2018
- use custom lookout_sanitize_url() function to sanitize URLs in customizer

= 1.0.1 =
* Released: January 12, 2018
- remove unnecessary widgets (author, ads),
- remove custom lookout_pagination( function)
- proper sanitize_callback in customizer.php


= 1.0 =
* Released: October 25,2017

Initial release