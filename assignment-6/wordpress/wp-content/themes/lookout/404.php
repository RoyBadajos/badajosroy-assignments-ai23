<?php 
/**
 * The template for displaying 404 pages (not found)
 */

get_header();
?>

<div class="container">

<div id="core">

    <div id="content" class="eightcol first ghost">
    
    	<div class="page">
            
            <div class="errorentry entry">

            	<h1 class="entry-title" itemprop="headline"><?php esc_html_e('Nothing found here','lookout');?></h1>
            
            	<h4><?php esc_html_e('Perhaps You will find something interesting from these lists...','lookout');?></h4>
            
            	<div class="hrline p-border"></div>
			
				<?php get_template_part('/inc/404-content');?>
            
            </div>
    
    	</div>
            
    </div><!-- #content -->

	<?php get_sidebar();?>
        
</div>
    
<?php get_footer(); ?>