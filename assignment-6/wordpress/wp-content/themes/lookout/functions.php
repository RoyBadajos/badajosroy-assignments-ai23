<?php

/**
 * Lookout functions and definitions
 */

/*-----------------------------------------------------------------------------------
- Default
----------------------------------------------------------------------------------- */

add_action( 'after_setup_theme', 'lookout_theme_setup' );

function lookout_theme_setup() {
	global $content_width;

	/* Set the $content_width for things such as video embeds. */
	if ( !isset( $content_width ) )
		$content_width = 1180;

	/* Add theme support */
	add_theme_support( 'post-formats', array( 'video','audio', 'gallery','quote', 'link', 'aside' ) );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'custom-background',array('default-color' => 'f7f7f7') );
	add_theme_support( 'title-tag' );
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );

	/* Add theme support for post thumbnails (featured images). */
	add_theme_support('post-thumbnails');
	add_image_size('lookout_slider', 977, 720, true ); //(cropped)
	add_image_size('lookout_small', 414, 322, true ); //(cropped)
	add_image_size('lookout_tabs_big', 303, 255, true ); //(cropped)
	add_image_size('lookout_tabs', 80, 80, true ); //(cropped)

	/* Add menus */
	register_nav_menus( array(
        'main-menu'   => esc_html__( 'Main Menu','lookout' ),
        'bottom-menu' => esc_html__( 'Footer Menu','lookout' ),
    ) );

	/* Add your sidebars function to the 'widgets_init' action hook. */
	add_action( 'widgets_init', 'lookout_register_sidebars' );

	// Make theme available for translation
	load_theme_textdomain('lookout', get_template_directory() . '/lang' );
	
	// header image
	$args = array(
		'flex-width'    => true,
		'width'         => 980,
		'flex-height'   => true,
		'height'        => 200,
		'default-image' => '',
		'header-text'   => false,
	);
	add_theme_support( 'custom-header', $args );
	
	// editor style
 	add_editor_style('styles/admin.css');
	
}

function lookout_register_sidebars() {
	
	register_sidebar(array('name' => esc_html__( 'Sidebar','lookout' ),'id' => 'tmnf-sidebar','description' => esc_html__( 'Sidebar widget section (displayed on posts / blog)','lookout' ),'before_widget' => '<div class="sidele ghost">','after_widget' => '</div>','before_title' => '<h2 class="widget">','after_title' => '</h2>'));
	
	register_sidebar(array('name' => esc_html__( 'Sidebar (Sticky)','lookout' ),'id' => 'tmnf-sidebar-sticky','description' => esc_html__( 'Sidebar widget section (displayed on posts / blog)','lookout' ),'before_widget' => '<div class="sidele ghost">','after_widget' => '</div>','before_title' => '<h2 class="widget">','after_title' => '</h2>'));
	
	//footer widgets
	register_sidebar(array('name' => esc_html__( 'Footer 1','lookout' ),'id' => 'tmnf-footer-1','description' => esc_html__( 'Widget section in footer - left','lookout' ),'before_widget' => '','after_widget' => '','before_title' => '<h2 class="widget dekoline">','after_title' => '</h2>'));
	register_sidebar(array('name' => esc_html__( 'Footer 2','lookout' ),'id' => 'tmnf-footer-2','description' => esc_html__( 'Widget section in footer - center/left','lookout' ),'before_widget' => '','after_widget' => '','before_title' => '<h2 class="widget dekoline">','after_title' => '</h2>'));
	register_sidebar(array('name' => esc_html__( 'Footer 3','lookout' ),'id' => 'tmnf-footer-3','description' => esc_html__( 'Widget section in footer - center/right','lookout' ),'before_widget' => '','after_widget' => '','before_title' => '<h2 class="widget dekoline">','after_title' => '</h2>'));
	register_sidebar(array('name' => esc_html__( 'Footer 4','lookout' ),'id' => 'tmnf-footer-4','description' => esc_html__( 'Widget section in footer - right','lookout' ),'before_widget' => '','after_widget' => '','before_title' => '<h2 class="widget dekoline">','after_title' => '</h2>'));
	
}

// customizer additions
require get_template_directory() . '/functions/customizer.php';
require get_template_directory() . '/functions/custom-controls.php';



function lookout_custom_logo_setup() {
    $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'lookout_custom_logo_setup' );


/*-----------------------------------------------------------------------------------
- Enqueues scripts and styles for front end
----------------------------------------------------------------------------------- */ 

function lookout_enqueue_style() {
	
	// main stylesheet
	wp_enqueue_style( 'lookout-default-style', get_stylesheet_uri());
	
	// Font Awesome css	
	wp_enqueue_style('font-awesome', get_template_directory_uri() .	'/styles/font-awesome.css');
	
	// mobile stylesheet
	wp_enqueue_style('lookout-mobile', get_template_directory_uri().'/style-mobile.css');
	
	// google link
	function lookout_fonts_url() {
		$font_url = add_query_arg( 'family', urlencode( 'Libre Franklin:400,400i,500,700|Montserrat:400,500,600,700,400italic&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese' ), "//fonts.googleapis.com/css" );
		return $font_url;
	}
	wp_enqueue_style( 'lookout-fonts', lookout_fonts_url(), array(), '1.0.0' );
	
}
add_action( 'wp_enqueue_scripts', 'lookout_enqueue_style' );


function lookout_enqueue_script() {

		// Load Common scripts
		wp_enqueue_script('jquery-scrolltofixed', get_template_directory_uri() .'/js/jquery-scrolltofixed.js',array( 'jquery' ),'', true);
		wp_enqueue_script('lookout-ownScript', get_template_directory_uri() .'/js/ownScript.js',array( 'jquery' ),'', true);
		

		// Singular comment script		
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );

}
	
add_action('wp_enqueue_scripts', 'lookout_enqueue_script');


/*-----------------------------------------------------------------------------------
- Include custom widgets
----------------------------------------------------------------------------------- */

include_once (get_template_directory() . '/functions/widgets/widget-featured.php');
include_once (get_template_directory() . '/functions/widgets/widget-featured-small.php');


	
/*-----------------------------------------------------------------------------------
- Other theme functions
----------------------------------------------------------------------------------- */


// icons - font awesome
function lookout_icon() {
	
	if(has_post_format('audio')) {return '<i title="'. esc_html__('Audio','lookout').'" class="tmnf_icon fa fa-volume-up"></i>';
	}elseif(has_post_format('gallery')) {return '<i title="'. esc_html__('Gallery','lookout').'" class="tmnf_icon fa fa-picture-o"></i>';
	}elseif(has_post_format('image')) {return '<i title="'. esc_html__('Image','lookout').'" class="tmnf_icon fa fa-camera"></i>';	
	}elseif(has_post_format('link')) {return '<i title="'. esc_html__('Link','lookout').'" class="tmnf_icon fa fa-link"></i>';			
	}elseif(has_post_format('quote')) {return '<i title="'. esc_html__('Quote','lookout').'" class="tmnf_icon fa fa-quote-right"></i>';		
	}elseif(has_post_format('video')) {return '<i title="'. esc_html__('Video','lookout').'" class="tmnf_icon fa fa-play"></i>';
	} else {}	
	
}

// automatically add prettyPhoto rel attributes to embedded images
function lookout_gallery_prettyPhoto ($content) {
	return str_replace("<a", "<a rel='prettyPhoto[gallery]'", $content);
}

function lookout_insert_prettyPhoto_rel($content) {
	$pattern = '/<a(.*?)href="(.*?).(bmp|gif|jpeg|jpg|png)"(.*?)>/i';
  	$replacement = '<a$1href="$2.$3" rel=\'prettyPhoto\'$4>';
	$content = preg_replace( $pattern, $replacement, $content );
	return $content;
}
add_filter( 'the_content', 'lookout_insert_prettyPhoto_rel' );
add_filter( 'wp_get_attachment_link', 'lookout_gallery_prettyPhoto');


// meta sections

function lookout_meta_cat() {
	?>    
	<p class="meta cat rad tranz ribbon">
		<?php the_category(' &bull; ') ?>
    </p>
    <?php
}

function lookout_meta_date() {
	?>    
	<p class="meta date tranz post-date"> 
        <?php the_time(get_option('date_format')); ?>
    </p>
    <?php
}

function lookout_meta_author() {
	?>    
	<p class="meta author tranz"> 
        <?php 
		echo get_avatar( get_the_author_meta('ID'), '37' );
		echo '<span>'; esc_html_e('Written by: ','lookout'); the_author_posts_link();echo '</span>';
		?>
    </p>
    
    <?php
}

function lookout_meta() { ?>   
	<p class="meta">
		<?php the_time(get_option('date_format')); ?> &bull; 
		<?php the_category(', ') ?>
    </p>
<?php }

function lookout_meta_full() { ?>    
	<p class="meta meta_full">
		<span itemprop="datePublished" class="post-date updated"><i class="icon-clock"></i> <?php the_time(get_option('date_format')); ?></span>
		<span class="categs"><i class="icon-folder-empty"></i> <?php the_category(', ') ?></span>
    </p>
<?php
}

function lookout_meta_more() {
	?>    
	<p class="meta_more rad">
    		<a class="rad p-border" href="<?php the_permalink() ?>"><?php esc_html_e('Read More','lookout');?> <i class="fa fa-angle-right"></i></a>
    </p>
    <?php
}

// site logo
if ( ! function_exists( 'lookout_site_logo' ) ) :
function lookout_site_logo() {

	if ( function_exists( 'the_custom_logo' ) ) {
    if(has_custom_logo()) {
        the_custom_logo();
    } else { ?>
    <?php }
	} 
} 
endif;


// footer text
function lookout_footer_text() {
	?>

	<span class="credit-link">
		<?php printf( esc_html__( 'Powered by %1$s and %2$s.', 'lookout' ),
			'<a href="https://wordpress.org" title="WordPress">WordPress</a>',
			'<a href="https://wpmasters.org/downloads/lookout-theme/" title="Lookout Theme">Lookout</a>'
		); ?>
	</span>

	<?php
}
add_action( 'lookout_footer_text', 'lookout_footer_text' );
?>